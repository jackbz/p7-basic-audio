//
//  Counter.hpp
//  JuceBasicAudio
//
//  Created by Jack on 13/11/2017.
//
//

#ifndef Counter_hpp
#define Counter_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"



class MyCounter: public Thread
{
public:
    MyCounter();
    
    ~MyCounter();
    
    void start();
    
    void stop();
    
    bool isItRunning();
    
    /** Class for counter listeners to inherit */
    class Listener
    {
    public:
        /** Destructor. */
        virtual ~Listener() {}
        /** Called when the next timer has reached the next interval. */
        virtual void counterChanged (const unsigned int counterValue) = 0;
    };
    
    void setListener (Listener* newListener);
    
    
private:
    void run() override;
    Listener* listener;
};
#endif /* Counter_hpp */
